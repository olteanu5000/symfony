<?php

/* SonataAdminBundle:CRUD:base_acl.html.twig */
class __TwigTemplate_dfdf395bc61d5d8d3db7e1a77c9ce5bcab516cfa25d2fd2afbad3b0e1f483251 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'form' => array($this, 'block_form'),
            'formactions' => array($this, 'block_formactions'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        // line 15
        echo "    <li>";
        $this->env->loadTemplate("SonataAdminBundle:Button:edit_button.html.twig")->display($context);
        echo "</li>
    <li>";
        // line 16
        $this->env->loadTemplate("SonataAdminBundle:Button:history_button.html.twig")->display($context);
        echo "</li>
    <li>";
        // line 17
        $this->env->loadTemplate("SonataAdminBundle:Button:show_button.html.twig")->display($context);
        echo "</li>
    <li>";
        // line 18
        $this->env->loadTemplate("SonataAdminBundle:Button:list_button.html.twig")->display($context);
        echo "</li>
";
    }

    // line 21
    public function block_form($context, array $blocks = array())
    {
        // line 22
        echo "    <form class=\"form-horizontal\"
              action=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateUrl", array(0 => "acl", 1 => array("id" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "uniqid" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "uniqid"), "subclass" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request"), "get", array(0 => "subclass"), "method"))), "method"), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo "
              method=\"POST\"
              ";
        // line 25
        if ((!$this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : $this->getContext($context, "admin_pool")), "getOption", array(0 => "html5_validate"), "method"))) {
            echo "novalidate=\"novalidate\"";
        }
        // line 26
        echo "              >
        ";
        // line 27
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars"), "errors")) > 0)) {
            // line 28
            echo "            <div class=\"sonata-ba-form-error\">
                ";
            // line 29
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            echo "
            </div>
        ";
        }
        // line 32
        echo "
        <table class=\"table\">
            <thead>
                <tr>
                    <th>";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("td_username", array(), "SonataAdminBundle"), "html", null, true);
        echo "</th>
                    ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["permissions"]) ? $context["permissions"] : $this->getContext($context, "permissions")));
        foreach ($context['_seq'] as $context["_key"] => $context["permission"]) {
            // line 38
            echo "                    <th>";
            echo twig_escape_filter($this->env, (isset($context["permission"]) ? $context["permission"] : $this->getContext($context, "permission")), "html", null, true);
            echo "</th>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['permission'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                </tr>
            </thead>
            <tbody>
            ";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 44
            echo "                <tr>
                    <td>";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "html", null, true);
            echo "</td>
                    ";
            // line 46
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["permissions"]) ? $context["permissions"] : $this->getContext($context, "permissions")));
            foreach ($context['_seq'] as $context["_key"] => $context["permission"]) {
                // line 47
                echo "                    <td>";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), ($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id") . (isset($context["permission"]) ? $context["permission"] : $this->getContext($context, "permission"))), array(), "array"), 'widget');
                echo "</td>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['permission'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "            </tbody>
        </table>

        ";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

        ";
        // line 56
        $this->displayBlock('formactions', $context, $blocks);
        // line 61
        echo "    </form>
";
    }

    // line 56
    public function block_formactions($context, array $blocks = array())
    {
        // line 57
        echo "            <div class=\"well well-small form-actions\">
                <input class=\"btn btn-primary\" type=\"submit\" name=\"btn_create_and_edit\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("btn_update_acl", array(), "SonataAdminBundle"), "html", null, true);
        echo "\">
            </div>
        ";
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_acl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1031 => 295,  1028 => 294,  1025 => 293,  1021 => 326,  1017 => 324,  1011 => 321,  1008 => 320,  1006 => 319,  1000 => 316,  992 => 315,  989 => 314,  987 => 313,  984 => 312,  978 => 310,  976 => 309,  973 => 308,  967 => 306,  965 => 305,  962 => 304,  956 => 302,  954 => 301,  951 => 300,  945 => 298,  943 => 297,  940 => 296,  938 => 293,  935 => 292,  932 => 291,  928 => 264,  922 => 261,  919 => 260,  916 => 259,  913 => 258,  909 => 285,  904 => 282,  896 => 277,  891 => 275,  887 => 273,  885 => 272,  881 => 270,  875 => 268,  873 => 267,  869 => 265,  867 => 258,  864 => 257,  861 => 256,  858 => 255,  853 => 286,  850 => 255,  847 => 254,  842 => 327,  840 => 291,  835 => 288,  833 => 254,  830 => 253,  827 => 252,  822 => 244,  819 => 243,  815 => 242,  811 => 240,  805 => 239,  800 => 236,  794 => 235,  782 => 233,  779 => 232,  775 => 231,  769 => 230,  762 => 227,  758 => 226,  750 => 224,  744 => 223,  741 => 222,  738 => 221,  735 => 220,  730 => 219,  727 => 218,  725 => 217,  722 => 216,  719 => 215,  712 => 214,  709 => 213,  706 => 212,  703 => 211,  697 => 210,  694 => 209,  691 => 208,  688 => 206,  681 => 205,  678 => 204,  672 => 203,  669 => 202,  665 => 201,  662 => 200,  659 => 199,  656 => 198,  650 => 197,  646 => 195,  632 => 186,  626 => 184,  623 => 183,  620 => 182,  616 => 246,  613 => 243,  610 => 198,  608 => 197,  605 => 196,  602 => 182,  599 => 181,  593 => 247,  591 => 181,  587 => 179,  584 => 178,  577 => 329,  575 => 252,  571 => 250,  569 => 178,  566 => 177,  563 => 176,  555 => 165,  552 => 164,  544 => 158,  541 => 157,  533 => 151,  530 => 150,  526 => 147,  522 => 145,  516 => 143,  513 => 142,  510 => 141,  496 => 140,  490 => 138,  486 => 136,  472 => 132,  470 => 131,  467 => 130,  464 => 129,  446 => 128,  443 => 127,  441 => 126,  438 => 125,  432 => 123,  428 => 172,  424 => 170,  422 => 150,  418 => 148,  416 => 123,  405 => 114,  399 => 112,  395 => 111,  391 => 109,  385 => 107,  382 => 106,  372 => 103,  367 => 102,  364 => 101,  361 => 100,  353 => 96,  349 => 94,  346 => 93,  338 => 112,  335 => 100,  333 => 93,  330 => 92,  327 => 91,  321 => 90,  317 => 87,  311 => 85,  297 => 84,  292 => 82,  289 => 81,  282 => 78,  279 => 77,  262 => 76,  259 => 75,  250 => 72,  248 => 71,  242 => 69,  239 => 68,  234 => 64,  231 => 63,  225 => 61,  222 => 60,  205 => 53,  192 => 50,  185 => 46,  181 => 44,  167 => 42,  175 => 43,  150 => 56,  58 => 19,  34 => 14,  114 => 34,  110 => 42,  65 => 15,  97 => 39,  81 => 32,  76 => 29,  53 => 22,  281 => 123,  256 => 74,  236 => 94,  232 => 93,  216 => 86,  211 => 56,  200 => 79,  195 => 77,  188 => 73,  184 => 72,  172 => 66,  152 => 61,  124 => 47,  186 => 62,  161 => 6,  23 => 13,  170 => 57,  160 => 57,  155 => 39,  134 => 32,  127 => 63,  113 => 44,  104 => 40,  100 => 91,  90 => 34,  84 => 38,  77 => 25,  70 => 27,  480 => 134,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 124,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 113,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 104,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 173,  337 => 103,  322 => 101,  314 => 86,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 79,  283 => 88,  278 => 86,  268 => 85,  264 => 115,  258 => 81,  252 => 109,  247 => 107,  241 => 77,  229 => 73,  220 => 87,  214 => 57,  177 => 65,  169 => 11,  140 => 51,  132 => 51,  128 => 30,  107 => 333,  61 => 27,  273 => 118,  269 => 117,  254 => 92,  243 => 105,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 91,  224 => 71,  221 => 77,  219 => 58,  217 => 75,  208 => 55,  204 => 80,  179 => 70,  159 => 41,  143 => 56,  135 => 69,  119 => 25,  102 => 175,  71 => 29,  67 => 26,  63 => 25,  59 => 13,  93 => 39,  88 => 33,  78 => 34,  94 => 34,  89 => 20,  85 => 32,  75 => 29,  68 => 14,  56 => 23,  27 => 16,  28 => 14,  87 => 36,  21 => 2,  201 => 92,  196 => 51,  183 => 61,  171 => 61,  166 => 10,  163 => 58,  158 => 5,  156 => 64,  151 => 50,  142 => 59,  138 => 33,  136 => 51,  121 => 59,  117 => 44,  105 => 176,  91 => 37,  62 => 20,  49 => 17,  38 => 6,  24 => 4,  46 => 12,  44 => 18,  25 => 12,  31 => 15,  26 => 13,  19 => 11,  79 => 31,  72 => 28,  69 => 23,  47 => 22,  40 => 17,  37 => 17,  22 => 11,  246 => 90,  157 => 56,  145 => 54,  139 => 45,  131 => 31,  123 => 52,  120 => 46,  115 => 42,  111 => 40,  108 => 38,  101 => 37,  98 => 36,  96 => 90,  83 => 24,  74 => 25,  66 => 23,  55 => 25,  52 => 23,  50 => 21,  43 => 18,  41 => 18,  35 => 22,  32 => 21,  29 => 16,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 13,  173 => 65,  168 => 69,  164 => 59,  162 => 68,  154 => 58,  149 => 51,  147 => 35,  144 => 59,  141 => 58,  133 => 49,  130 => 41,  125 => 46,  122 => 26,  116 => 45,  112 => 56,  109 => 43,  106 => 36,  103 => 38,  99 => 30,  95 => 38,  92 => 88,  86 => 66,  82 => 33,  80 => 20,  73 => 24,  64 => 21,  60 => 26,  57 => 20,  54 => 16,  51 => 15,  48 => 13,  45 => 8,  42 => 20,  39 => 19,  36 => 16,  33 => 4,  30 => 17,);
    }
}
