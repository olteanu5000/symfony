<?php

/* SonataDoctrineORMAdminBundle:Block:block_audit.html.twig */
class __TwigTemplate_5658c069e9e0d5cd5685dfddbb18aec3eda52ca8cb17c2aab6aa86ba721c2c65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SonataBlockBundle:Block:block_base.html.twig");

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataBlockBundle:Block:block_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 13
    public function block_block($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"sonata-news-block-recent-post panel panel-default\">
        <div class=\"panel-heading\">
            <h3 class=\"sonata-news-block-recent-post panel-title\"><i
                        class=\"fa fa-history\"></i> ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("title_audit_log", array(), "SonataAdminBundle"), "html", null, true);
        echo "</h3>
        </div>
        <h3></h3>


        <div class=\"panel-group\" id=\"accordion\">
            ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["revisions"]) ? $context["revisions"] : $this->getContext($context, "revisions")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["revision"]) {
            // line 24
            echo "                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h4 class=\"panel-title\">
                            <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
            echo "\">
                                ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["revision"]) ? $context["revision"] : $this->getContext($context, "revision")), "revision"), "rev"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["revision"]) ? $context["revision"] : $this->getContext($context, "revision")), "revision"), "username"), "html", null, true);
            echo "
                                - ";
            // line 29
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["revision"]) ? $context["revision"] : $this->getContext($context, "revision")), "revision"), "timestamp")), "html", null, true);
            echo "
                            </a>
                        </h4>
                    </div>
                    <div id=\"collapse";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
            echo "\" class=\"panel-collapse collapse ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "first")) ? ("in") : (""));
            echo "\">
                        <div class=\"panel-body\">
                            <ul>
                                ";
            // line 36
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["revision"]) ? $context["revision"] : $this->getContext($context, "revision")), "entities"));
            foreach ($context['_seq'] as $context["_key"] => $context["changedEntity"]) {
                // line 37
                echo "                                    <li>
                                        ";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["changedEntity"]) ? $context["changedEntity"] : $this->getContext($context, "changedEntity")), "entity"), "html", null, true);
                echo " / ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["changedEntity"]) ? $context["changedEntity"] : $this->getContext($context, "changedEntity")), "revisionType"), "html", null, true);
                echo "
                                        / ";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["changedEntity"]) ? $context["changedEntity"] : $this->getContext($context, "changedEntity")), "className"), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["changedEntity"]) ? $context["changedEntity"] : $this->getContext($context, "changedEntity")), "id"), "html", null, true);
                echo "
                                    </li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['changedEntity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "                            </ul>
                        </div>
                    </div>
                </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['revision'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:Block:block_audit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  358 => 139,  347 => 134,  323 => 125,  251 => 101,  228 => 88,  213 => 82,  267 => 5,  218 => 97,  244 => 111,  1102 => 321,  1096 => 318,  1086 => 376,  1079 => 374,  1076 => 372,  1060 => 370,  1054 => 369,  1037 => 368,  1033 => 366,  979 => 323,  974 => 321,  968 => 318,  958 => 310,  941 => 307,  918 => 305,  915 => 304,  905 => 300,  901 => 299,  895 => 296,  859 => 280,  855 => 279,  851 => 278,  837 => 272,  825 => 269,  818 => 267,  808 => 263,  790 => 254,  788 => 253,  781 => 250,  766 => 248,  763 => 247,  760 => 246,  757 => 245,  755 => 244,  724 => 233,  721 => 232,  682 => 227,  677 => 226,  671 => 224,  661 => 218,  654 => 216,  585 => 200,  574 => 195,  561 => 171,  549 => 167,  536 => 187,  534 => 186,  531 => 185,  517 => 179,  514 => 178,  491 => 174,  489 => 167,  463 => 158,  460 => 157,  454 => 156,  450 => 155,  429 => 149,  420 => 146,  417 => 145,  414 => 144,  411 => 143,  369 => 125,  366 => 123,  351 => 135,  325 => 110,  319 => 124,  316 => 107,  307 => 105,  304 => 104,  280 => 94,  277 => 109,  206 => 54,  148 => 60,  1035 => 367,  1026 => 311,  1023 => 359,  1009 => 307,  1003 => 344,  953 => 301,  950 => 300,  942 => 296,  926 => 287,  924 => 286,  920 => 285,  917 => 284,  912 => 280,  900 => 277,  898 => 276,  893 => 274,  884 => 270,  880 => 267,  877 => 265,  872 => 284,  865 => 259,  863 => 281,  841 => 257,  838 => 255,  831 => 271,  828 => 250,  824 => 247,  820 => 245,  817 => 244,  813 => 265,  810 => 264,  806 => 235,  804 => 234,  802 => 233,  799 => 258,  795 => 229,  793 => 255,  791 => 227,  789 => 226,  787 => 225,  784 => 251,  780 => 221,  777 => 216,  772 => 212,  752 => 208,  749 => 206,  747 => 205,  742 => 202,  739 => 200,  737 => 199,  732 => 238,  728 => 192,  726 => 191,  723 => 190,  717 => 186,  714 => 185,  704 => 230,  701 => 180,  699 => 179,  696 => 178,  692 => 175,  690 => 174,  687 => 229,  683 => 170,  674 => 225,  664 => 160,  655 => 155,  653 => 154,  644 => 149,  641 => 148,  634 => 144,  630 => 141,  628 => 140,  621 => 136,  619 => 135,  611 => 211,  601 => 128,  596 => 127,  594 => 205,  589 => 123,  579 => 197,  578 => 115,  576 => 196,  567 => 173,  559 => 104,  557 => 103,  553 => 101,  551 => 100,  547 => 99,  542 => 189,  539 => 95,  525 => 182,  508 => 88,  505 => 87,  477 => 80,  475 => 163,  473 => 78,  466 => 159,  449 => 75,  442 => 71,  426 => 148,  400 => 55,  397 => 54,  376 => 129,  373 => 45,  363 => 38,  345 => 117,  340 => 27,  329 => 21,  320 => 17,  303 => 13,  301 => 117,  290 => 5,  266 => 90,  260 => 104,  255 => 283,  245 => 269,  215 => 224,  210 => 97,  207 => 91,  191 => 194,  212 => 58,  197 => 74,  174 => 67,  165 => 64,  153 => 55,  648 => 236,  637 => 214,  633 => 233,  625 => 139,  614 => 226,  612 => 225,  607 => 222,  597 => 206,  590 => 214,  586 => 122,  583 => 211,  581 => 198,  572 => 112,  568 => 207,  564 => 108,  560 => 205,  554 => 204,  545 => 200,  538 => 197,  535 => 196,  532 => 195,  527 => 186,  523 => 172,  520 => 180,  506 => 161,  502 => 160,  499 => 159,  494 => 175,  483 => 127,  479 => 126,  468 => 77,  459 => 119,  456 => 118,  433 => 96,  421 => 131,  419 => 118,  415 => 60,  410 => 94,  401 => 137,  359 => 120,  348 => 118,  343 => 132,  334 => 134,  331 => 22,  328 => 111,  291 => 80,  288 => 4,  265 => 106,  253 => 85,  237 => 109,  202 => 77,  180 => 66,  392 => 107,  389 => 133,  383 => 104,  377 => 153,  354 => 95,  352 => 34,  342 => 116,  332 => 88,  326 => 19,  324 => 18,  318 => 83,  315 => 123,  302 => 103,  299 => 116,  293 => 113,  287 => 71,  284 => 70,  276 => 305,  271 => 108,  263 => 4,  257 => 103,  233 => 107,  194 => 66,  190 => 69,  146 => 58,  137 => 29,  129 => 59,  126 => 55,  12 => 36,  118 => 46,  20 => 11,  1031 => 295,  1028 => 294,  1025 => 293,  1021 => 326,  1017 => 324,  1011 => 321,  1008 => 320,  1006 => 319,  1000 => 305,  992 => 315,  989 => 314,  987 => 313,  984 => 312,  978 => 302,  976 => 309,  973 => 308,  967 => 306,  965 => 305,  962 => 304,  956 => 302,  954 => 301,  951 => 300,  945 => 298,  943 => 297,  940 => 296,  938 => 295,  935 => 306,  932 => 291,  928 => 264,  922 => 261,  919 => 260,  916 => 259,  913 => 258,  909 => 301,  904 => 278,  896 => 275,  891 => 275,  887 => 293,  885 => 272,  881 => 290,  875 => 264,  873 => 267,  869 => 265,  867 => 282,  864 => 257,  861 => 256,  858 => 255,  853 => 286,  850 => 255,  847 => 277,  842 => 275,  840 => 291,  835 => 253,  833 => 252,  830 => 253,  827 => 252,  822 => 268,  819 => 243,  815 => 242,  811 => 240,  805 => 262,  800 => 236,  794 => 235,  782 => 233,  779 => 232,  775 => 231,  769 => 249,  762 => 227,  758 => 226,  750 => 242,  744 => 203,  741 => 222,  738 => 240,  735 => 239,  730 => 219,  727 => 218,  725 => 217,  722 => 216,  719 => 187,  712 => 214,  709 => 213,  706 => 212,  703 => 211,  697 => 210,  694 => 209,  691 => 208,  688 => 206,  681 => 169,  678 => 168,  672 => 164,  669 => 163,  665 => 220,  662 => 159,  659 => 217,  656 => 198,  650 => 153,  646 => 150,  632 => 186,  626 => 184,  623 => 183,  620 => 213,  616 => 212,  613 => 243,  610 => 198,  608 => 210,  605 => 209,  602 => 208,  599 => 207,  593 => 247,  591 => 124,  587 => 179,  584 => 178,  577 => 114,  575 => 209,  571 => 194,  569 => 110,  566 => 177,  563 => 176,  555 => 169,  552 => 168,  544 => 97,  541 => 198,  533 => 151,  530 => 150,  526 => 147,  522 => 181,  516 => 170,  513 => 169,  510 => 168,  496 => 176,  490 => 138,  486 => 136,  472 => 122,  470 => 131,  467 => 130,  464 => 120,  446 => 74,  443 => 154,  441 => 153,  438 => 152,  432 => 150,  428 => 64,  424 => 62,  422 => 150,  418 => 148,  416 => 123,  405 => 58,  399 => 168,  395 => 135,  391 => 109,  385 => 48,  382 => 131,  372 => 127,  367 => 149,  364 => 122,  361 => 121,  353 => 96,  349 => 33,  346 => 93,  338 => 130,  335 => 89,  333 => 23,  330 => 92,  327 => 126,  321 => 109,  317 => 16,  311 => 85,  297 => 84,  292 => 82,  289 => 112,  282 => 69,  279 => 68,  262 => 105,  259 => 118,  250 => 113,  248 => 100,  242 => 101,  239 => 97,  234 => 64,  231 => 99,  225 => 87,  222 => 237,  205 => 90,  192 => 90,  185 => 61,  181 => 46,  167 => 56,  175 => 43,  150 => 71,  58 => 28,  34 => 4,  114 => 51,  110 => 22,  65 => 27,  97 => 47,  81 => 31,  76 => 27,  53 => 10,  281 => 110,  256 => 74,  236 => 94,  232 => 93,  216 => 60,  211 => 81,  200 => 94,  195 => 51,  188 => 73,  184 => 47,  172 => 59,  152 => 72,  124 => 57,  186 => 81,  161 => 162,  23 => 11,  170 => 71,  160 => 50,  155 => 55,  134 => 47,  127 => 54,  113 => 44,  104 => 45,  100 => 43,  90 => 20,  84 => 33,  77 => 29,  70 => 29,  480 => 166,  474 => 161,  469 => 160,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 70,  437 => 147,  435 => 151,  430 => 65,  427 => 143,  423 => 147,  413 => 59,  409 => 132,  407 => 93,  402 => 56,  398 => 136,  393 => 51,  387 => 132,  384 => 158,  381 => 47,  379 => 130,  374 => 101,  368 => 41,  365 => 141,  362 => 147,  360 => 37,  355 => 145,  341 => 131,  337 => 26,  322 => 101,  314 => 86,  312 => 106,  309 => 120,  305 => 119,  298 => 11,  294 => 81,  285 => 111,  283 => 95,  278 => 309,  268 => 107,  264 => 115,  258 => 86,  252 => 3,  247 => 107,  241 => 77,  229 => 105,  220 => 232,  214 => 75,  177 => 45,  169 => 66,  140 => 52,  132 => 27,  128 => 47,  107 => 52,  61 => 28,  273 => 92,  269 => 6,  254 => 102,  243 => 98,  240 => 262,  238 => 84,  235 => 108,  230 => 61,  227 => 104,  224 => 103,  221 => 77,  219 => 84,  217 => 75,  208 => 96,  204 => 78,  179 => 76,  159 => 66,  143 => 59,  135 => 69,  119 => 24,  102 => 44,  71 => 28,  67 => 27,  63 => 22,  59 => 23,  93 => 45,  88 => 37,  78 => 16,  94 => 35,  89 => 43,  85 => 31,  75 => 34,  68 => 31,  56 => 23,  27 => 13,  28 => 13,  87 => 35,  21 => 11,  201 => 52,  196 => 56,  183 => 78,  171 => 57,  166 => 167,  163 => 42,  158 => 62,  156 => 64,  151 => 36,  142 => 31,  138 => 57,  136 => 58,  121 => 107,  117 => 57,  105 => 39,  91 => 34,  62 => 24,  49 => 9,  38 => 6,  24 => 3,  46 => 19,  44 => 18,  25 => 12,  31 => 14,  26 => 14,  19 => 1,  79 => 37,  72 => 33,  69 => 31,  47 => 17,  40 => 19,  37 => 18,  22 => 2,  246 => 99,  157 => 51,  145 => 60,  139 => 59,  131 => 55,  123 => 48,  120 => 47,  115 => 45,  111 => 43,  108 => 42,  101 => 25,  98 => 37,  96 => 37,  83 => 32,  74 => 23,  66 => 29,  55 => 24,  52 => 43,  50 => 22,  43 => 14,  41 => 19,  35 => 5,  32 => 16,  29 => 15,  209 => 92,  203 => 89,  199 => 86,  193 => 83,  189 => 71,  187 => 48,  182 => 69,  176 => 58,  173 => 72,  168 => 70,  164 => 55,  162 => 54,  154 => 37,  149 => 62,  147 => 34,  144 => 48,  141 => 55,  133 => 46,  130 => 57,  125 => 47,  122 => 25,  116 => 42,  112 => 176,  109 => 53,  106 => 86,  103 => 50,  99 => 38,  95 => 41,  92 => 36,  86 => 41,  82 => 28,  80 => 17,  73 => 27,  64 => 28,  60 => 15,  57 => 25,  54 => 23,  51 => 22,  48 => 21,  45 => 23,  42 => 7,  39 => 17,  36 => 17,  33 => 3,  30 => 15,);
    }
}
