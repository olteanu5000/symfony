<?php

/* SonataDoctrineORMAdminBundle:CRUD:edit_orm_many_to_one.html.twig */
class __TwigTemplate_9bc6257e253e4c0fae0df7d6332923b31f812dc40ab53be2f5b38862df523901 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 11
        echo "
";
        // line 12
        if ((!$this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "hasassociationadmin"))) {
            // line 13
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('sonata_admin')->renderRelationElement((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description")), "html", null, true);
            echo "
";
        } elseif (($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "edit") == "inline")) {
            // line 15
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "formfielddescriptions"));
            foreach ($context['_seq'] as $context["_key"] => $context["field_description"]) {
                // line 16
                echo "        ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), $this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "name"), array(), "array"), 'row');
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field_description'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 19
            echo "    <div id=\"field_container_";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "\" class=\"field-container\">
        ";
            // line 20
            if (($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "edit") == "list")) {
                // line 21
                echo "            <span id=\"field_widget_";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo "\" class=\"field-short-description\">
                ";
                // line 22
                if ($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "admin"), "id", array(0 => $this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "value")), "method")) {
                    // line 23
                    echo "                    ";
                    echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('routing')->getUrl("sonata_admin_short_object_information", array("code" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "code"), "objectId" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "id", array(0 => $this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "value")), "method"), "uniqid" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "uniqid"), "linkParameters" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "options"), "link_parameters"))), array());
                    // line 29
                    echo "                ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : null), "field_description", array(), "any", false, true), "options", array(), "any", false, true), "placeholder", array(), "any", true, true) && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "options"), "placeholder"))) {
                    // line 30
                    echo "                    <span class=\"inner-field-short-description\">
                        ";
                    // line 31
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "options"), "placeholder"), array(), "SonataAdminBundle"), "html", null, true);
                    echo "
                    </span>
                ";
                }
                // line 34
                echo "            </span>
            <span style=\"display: none\" >
                ";
                // line 36
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
                echo "
            </span>
        ";
            } else {
                // line 39
                echo "            <span id=\"field_widget_";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo "\" >
                ";
                // line 40
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
                echo "
            </span>
        ";
            }
            // line 43
            echo "
        <span id=\"field_actions_";
            // line 44
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "\" class=\"field-actions\">
            <span class=\"btn-group\">
                ";
            // line 46
            if ((((($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "edit") == "list") && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "hasRoute", array(0 => "list"), "method")) && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "isGranted", array(0 => "LIST"), "method")) && (isset($context["btn_list"]) ? $context["btn_list"] : $this->getContext($context, "btn_list")))) {
                // line 47
                echo "                    <a  href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "generateUrl", array(0 => "list", 1 => $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "getOption", array(0 => "link_parameters", 1 => array()), "method")), "method"), "html", null, true);
                echo "\"
                        onclick=\"return start_field_dialog_form_list_";
                // line 48
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo "(this);\"
                        class=\"btn btn-info btn-sm btn-outline sonata-ba-action\"
                        title=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["btn_list"]) ? $context["btn_list"] : $this->getContext($context, "btn_list")), array(), (isset($context["btn_catalogue"]) ? $context["btn_catalogue"] : $this->getContext($context, "btn_catalogue"))), "html", null, true);
                echo "\"
                        >
                        <i class=\"fa fa-list\"></i>
                        ";
                // line 53
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["btn_list"]) ? $context["btn_list"] : $this->getContext($context, "btn_list")), array(), (isset($context["btn_catalogue"]) ? $context["btn_catalogue"] : $this->getContext($context, "btn_catalogue"))), "html", null, true);
                echo "
                    </a>
                ";
            }
            // line 56
            echo "
                ";
            // line 57
            if ((((($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "edit") != "admin") && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "hasRoute", array(0 => "create"), "method")) && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "isGranted", array(0 => "CREATE"), "method")) && (isset($context["btn_add"]) ? $context["btn_add"] : $this->getContext($context, "btn_add")))) {
                // line 58
                echo "                    <a  href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "generateUrl", array(0 => "create", 1 => $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "getOption", array(0 => "link_parameters", 1 => array()), "method")), "method"), "html", null, true);
                echo "\"
                        onclick=\"return start_field_dialog_form_add_";
                // line 59
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo "(this);\"
                        class=\"btn btn-success btn-sm btn-outline sonata-ba-action\"
                        title=\"";
                // line 61
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["btn_add"]) ? $context["btn_add"] : $this->getContext($context, "btn_add")), array(), (isset($context["btn_catalogue"]) ? $context["btn_catalogue"] : $this->getContext($context, "btn_catalogue"))), "html", null, true);
                echo "\"
                        >
                        <i class=\"fa fa-plus-circle\"></i>
                        ";
                // line 64
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["btn_add"]) ? $context["btn_add"] : $this->getContext($context, "btn_add")), array(), (isset($context["btn_catalogue"]) ? $context["btn_catalogue"] : $this->getContext($context, "btn_catalogue"))), "html", null, true);
                echo "
                    </a>
                ";
            }
            // line 67
            echo "            </span>

            <span class=\"btn-group\">
                ";
            // line 70
            if ((((($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "edit") == "list") && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "hasRoute", array(0 => "list"), "method")) && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "field_description"), "associationadmin"), "isGranted", array(0 => "LIST"), "method")) && (isset($context["btn_delete"]) ? $context["btn_delete"] : $this->getContext($context, "btn_delete")))) {
                // line 71
                echo "                    <a  href=\"\"
                        onclick=\"return remove_selected_element_";
                // line 72
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo "(this);\"
                        class=\"btn btn-danger btn-sm btn-outline sonata-ba-action\"
                        title=\"";
                // line 74
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["btn_delete"]) ? $context["btn_delete"] : $this->getContext($context, "btn_delete")), array(), (isset($context["btn_catalogue"]) ? $context["btn_catalogue"] : $this->getContext($context, "btn_catalogue"))), "html", null, true);
                echo "\"
                        >
                        <i class=\"fa fa-minus-circle\"></i>
                        ";
                // line 77
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["btn_delete"]) ? $context["btn_delete"] : $this->getContext($context, "btn_delete")), array(), (isset($context["btn_catalogue"]) ? $context["btn_catalogue"] : $this->getContext($context, "btn_catalogue"))), "html", null, true);
                echo "
                    </a>
                ";
            }
            // line 80
            echo "            </span>
        </span>

        ";
            // line 83
            $this->env->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:edit_modal.html.twig")->display($context);
            // line 84
            echo "    </div>

    ";
            // line 86
            $this->env->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:edit_orm_many_association_script.html.twig")->display($context);
        }
    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:edit_orm_many_to_one.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  558 => 186,  543 => 179,  537 => 176,  528 => 173,  518 => 170,  511 => 167,  501 => 161,  495 => 158,  487 => 156,  455 => 141,  439 => 133,  436 => 132,  403 => 117,  380 => 107,  308 => 86,  272 => 81,  249 => 74,  358 => 103,  347 => 134,  323 => 125,  251 => 101,  228 => 88,  213 => 69,  267 => 78,  218 => 97,  244 => 111,  1102 => 321,  1096 => 318,  1086 => 376,  1079 => 374,  1076 => 372,  1060 => 370,  1054 => 369,  1037 => 368,  1033 => 366,  979 => 323,  974 => 321,  968 => 318,  958 => 310,  941 => 307,  918 => 305,  915 => 304,  905 => 300,  901 => 299,  895 => 296,  859 => 280,  855 => 279,  851 => 278,  837 => 272,  825 => 269,  818 => 267,  808 => 263,  790 => 254,  788 => 253,  781 => 250,  766 => 248,  763 => 247,  760 => 246,  757 => 245,  755 => 244,  724 => 233,  721 => 232,  682 => 227,  677 => 226,  671 => 224,  661 => 218,  654 => 216,  585 => 200,  574 => 195,  561 => 171,  549 => 182,  536 => 187,  534 => 186,  531 => 185,  517 => 179,  514 => 168,  491 => 157,  489 => 167,  463 => 158,  460 => 143,  454 => 156,  450 => 155,  429 => 149,  420 => 123,  417 => 145,  414 => 144,  411 => 120,  369 => 125,  366 => 106,  351 => 135,  325 => 94,  319 => 124,  316 => 107,  307 => 105,  304 => 85,  280 => 94,  277 => 109,  206 => 54,  148 => 64,  1035 => 367,  1026 => 311,  1023 => 359,  1009 => 307,  1003 => 344,  953 => 301,  950 => 300,  942 => 296,  926 => 287,  924 => 286,  920 => 285,  917 => 284,  912 => 280,  900 => 277,  898 => 276,  893 => 274,  884 => 270,  880 => 267,  877 => 265,  872 => 284,  865 => 259,  863 => 281,  841 => 257,  838 => 255,  831 => 271,  828 => 250,  824 => 247,  820 => 245,  817 => 244,  813 => 265,  810 => 264,  806 => 235,  804 => 234,  802 => 233,  799 => 258,  795 => 229,  793 => 255,  791 => 227,  789 => 226,  787 => 225,  784 => 251,  780 => 221,  777 => 216,  772 => 212,  752 => 208,  749 => 206,  747 => 205,  742 => 202,  739 => 200,  737 => 199,  732 => 238,  728 => 192,  726 => 191,  723 => 190,  717 => 186,  714 => 185,  704 => 230,  701 => 180,  699 => 179,  696 => 178,  692 => 175,  690 => 174,  687 => 229,  683 => 170,  674 => 225,  664 => 160,  655 => 155,  653 => 154,  644 => 149,  641 => 148,  634 => 144,  630 => 141,  628 => 140,  621 => 136,  619 => 135,  611 => 211,  601 => 128,  596 => 127,  594 => 205,  589 => 123,  579 => 197,  578 => 115,  576 => 196,  567 => 173,  559 => 104,  557 => 103,  553 => 184,  551 => 100,  547 => 99,  542 => 189,  539 => 95,  525 => 172,  508 => 165,  505 => 87,  477 => 80,  475 => 163,  473 => 78,  466 => 159,  449 => 138,  442 => 134,  426 => 126,  400 => 116,  397 => 54,  376 => 129,  373 => 45,  363 => 38,  345 => 117,  340 => 27,  329 => 21,  320 => 92,  303 => 13,  301 => 117,  290 => 5,  266 => 90,  260 => 104,  255 => 283,  245 => 269,  215 => 224,  210 => 97,  207 => 91,  191 => 194,  212 => 58,  197 => 74,  174 => 59,  165 => 72,  153 => 55,  648 => 236,  637 => 214,  633 => 233,  625 => 139,  614 => 226,  612 => 225,  607 => 222,  597 => 206,  590 => 214,  586 => 122,  583 => 211,  581 => 198,  572 => 112,  568 => 207,  564 => 108,  560 => 187,  554 => 204,  545 => 200,  538 => 197,  535 => 196,  532 => 174,  527 => 186,  523 => 171,  520 => 180,  506 => 161,  502 => 160,  499 => 159,  494 => 175,  483 => 127,  479 => 126,  468 => 77,  459 => 119,  456 => 118,  433 => 130,  421 => 131,  419 => 118,  415 => 121,  410 => 94,  401 => 137,  359 => 120,  348 => 118,  343 => 132,  334 => 134,  331 => 96,  328 => 111,  291 => 80,  288 => 4,  265 => 106,  253 => 85,  237 => 109,  202 => 77,  180 => 66,  392 => 107,  389 => 133,  383 => 104,  377 => 153,  354 => 101,  352 => 34,  342 => 116,  332 => 88,  326 => 19,  324 => 18,  318 => 83,  315 => 123,  302 => 103,  299 => 116,  293 => 113,  287 => 71,  284 => 70,  276 => 305,  271 => 108,  263 => 4,  257 => 103,  233 => 71,  194 => 66,  190 => 69,  146 => 49,  137 => 59,  129 => 57,  126 => 42,  12 => 36,  118 => 46,  20 => 11,  1031 => 295,  1028 => 294,  1025 => 293,  1021 => 326,  1017 => 324,  1011 => 321,  1008 => 320,  1006 => 319,  1000 => 305,  992 => 315,  989 => 314,  987 => 313,  984 => 312,  978 => 302,  976 => 309,  973 => 308,  967 => 306,  965 => 305,  962 => 304,  956 => 302,  954 => 301,  951 => 300,  945 => 298,  943 => 297,  940 => 296,  938 => 295,  935 => 306,  932 => 291,  928 => 264,  922 => 261,  919 => 260,  916 => 259,  913 => 258,  909 => 301,  904 => 278,  896 => 275,  891 => 275,  887 => 293,  885 => 272,  881 => 290,  875 => 264,  873 => 267,  869 => 265,  867 => 282,  864 => 257,  861 => 256,  858 => 255,  853 => 286,  850 => 255,  847 => 277,  842 => 275,  840 => 291,  835 => 253,  833 => 252,  830 => 253,  827 => 252,  822 => 268,  819 => 243,  815 => 242,  811 => 240,  805 => 262,  800 => 236,  794 => 235,  782 => 233,  779 => 232,  775 => 231,  769 => 249,  762 => 227,  758 => 226,  750 => 242,  744 => 203,  741 => 222,  738 => 240,  735 => 239,  730 => 219,  727 => 218,  725 => 217,  722 => 216,  719 => 187,  712 => 214,  709 => 213,  706 => 212,  703 => 211,  697 => 210,  694 => 209,  691 => 208,  688 => 206,  681 => 169,  678 => 168,  672 => 164,  669 => 163,  665 => 220,  662 => 159,  659 => 217,  656 => 198,  650 => 153,  646 => 150,  632 => 186,  626 => 184,  623 => 183,  620 => 213,  616 => 212,  613 => 243,  610 => 198,  608 => 210,  605 => 209,  602 => 208,  599 => 207,  593 => 247,  591 => 124,  587 => 179,  584 => 178,  577 => 114,  575 => 209,  571 => 194,  569 => 110,  566 => 177,  563 => 188,  555 => 185,  552 => 168,  544 => 97,  541 => 198,  533 => 151,  530 => 150,  526 => 147,  522 => 181,  516 => 170,  513 => 169,  510 => 168,  496 => 176,  490 => 138,  486 => 136,  472 => 122,  470 => 131,  467 => 130,  464 => 120,  446 => 74,  443 => 154,  441 => 153,  438 => 152,  432 => 150,  428 => 64,  424 => 62,  422 => 150,  418 => 148,  416 => 123,  405 => 118,  399 => 168,  395 => 135,  391 => 109,  385 => 48,  382 => 131,  372 => 127,  367 => 149,  364 => 122,  361 => 121,  353 => 96,  349 => 33,  346 => 93,  338 => 130,  335 => 89,  333 => 23,  330 => 92,  327 => 126,  321 => 109,  317 => 91,  311 => 87,  297 => 84,  292 => 82,  289 => 82,  282 => 69,  279 => 68,  262 => 105,  259 => 118,  250 => 113,  248 => 100,  242 => 101,  239 => 97,  234 => 64,  231 => 99,  225 => 87,  222 => 237,  205 => 90,  192 => 86,  185 => 61,  181 => 80,  167 => 57,  175 => 77,  150 => 65,  58 => 28,  34 => 16,  114 => 51,  110 => 48,  65 => 30,  97 => 47,  81 => 30,  76 => 27,  53 => 10,  281 => 110,  256 => 74,  236 => 94,  232 => 93,  216 => 70,  211 => 81,  200 => 94,  195 => 51,  188 => 84,  184 => 47,  172 => 59,  152 => 51,  124 => 41,  186 => 83,  161 => 71,  23 => 11,  170 => 74,  160 => 70,  155 => 52,  134 => 45,  127 => 56,  113 => 44,  104 => 45,  100 => 43,  90 => 20,  84 => 39,  77 => 29,  70 => 29,  480 => 166,  474 => 150,  469 => 160,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 70,  437 => 147,  435 => 151,  430 => 65,  427 => 143,  423 => 147,  413 => 59,  409 => 132,  407 => 93,  402 => 56,  398 => 115,  393 => 112,  387 => 110,  384 => 109,  381 => 47,  379 => 130,  374 => 101,  368 => 41,  365 => 141,  362 => 147,  360 => 104,  355 => 145,  341 => 131,  337 => 97,  322 => 93,  314 => 88,  312 => 106,  309 => 120,  305 => 119,  298 => 11,  294 => 81,  285 => 111,  283 => 95,  278 => 309,  268 => 107,  264 => 115,  258 => 86,  252 => 3,  247 => 107,  241 => 77,  229 => 105,  220 => 232,  214 => 75,  177 => 45,  169 => 74,  140 => 47,  132 => 58,  128 => 47,  107 => 52,  61 => 28,  273 => 92,  269 => 6,  254 => 102,  243 => 98,  240 => 72,  238 => 84,  235 => 108,  230 => 61,  227 => 104,  224 => 103,  221 => 77,  219 => 84,  217 => 75,  208 => 96,  204 => 78,  179 => 76,  159 => 70,  143 => 59,  135 => 35,  119 => 28,  102 => 38,  71 => 28,  67 => 27,  63 => 22,  59 => 23,  93 => 34,  88 => 37,  78 => 36,  94 => 35,  89 => 40,  85 => 31,  75 => 34,  68 => 31,  56 => 23,  27 => 13,  28 => 14,  87 => 33,  21 => 12,  201 => 52,  196 => 68,  183 => 78,  171 => 57,  166 => 167,  163 => 42,  158 => 53,  156 => 68,  151 => 36,  142 => 61,  138 => 36,  136 => 58,  121 => 53,  117 => 51,  105 => 47,  91 => 34,  62 => 29,  49 => 20,  38 => 6,  24 => 13,  46 => 19,  44 => 18,  25 => 12,  31 => 15,  26 => 13,  19 => 11,  79 => 37,  72 => 25,  69 => 24,  47 => 19,  40 => 19,  37 => 17,  22 => 12,  246 => 99,  157 => 51,  145 => 60,  139 => 60,  131 => 55,  123 => 54,  120 => 47,  115 => 50,  111 => 43,  108 => 48,  101 => 25,  98 => 44,  96 => 37,  83 => 32,  74 => 34,  66 => 29,  55 => 22,  52 => 21,  50 => 20,  43 => 18,  41 => 18,  35 => 16,  32 => 16,  29 => 15,  209 => 92,  203 => 89,  199 => 86,  193 => 83,  189 => 65,  187 => 48,  182 => 80,  176 => 77,  173 => 72,  168 => 70,  164 => 72,  162 => 71,  154 => 67,  149 => 62,  147 => 34,  144 => 62,  141 => 55,  133 => 46,  130 => 57,  125 => 47,  122 => 25,  116 => 42,  112 => 49,  109 => 40,  106 => 86,  103 => 46,  99 => 26,  95 => 43,  92 => 36,  86 => 41,  82 => 28,  80 => 17,  73 => 29,  64 => 26,  60 => 25,  57 => 22,  54 => 18,  51 => 21,  48 => 21,  45 => 19,  42 => 18,  39 => 17,  36 => 16,  33 => 3,  30 => 15,);
    }
}
