<?php

/* SonataAdminBundle:CRUD:show_choice.html.twig */
class __TwigTemplate_90b4a142ee68e8cc489acbd469c731b5cbd4cab55310b028faeedf05aa549b8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig");

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 13
    public function block_field($context, array $blocks = array())
    {
        // line 14
        ob_start();
        // line 15
        echo "    ";
        if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "choices", array(), "any", true, true)) {
            // line 16
            echo "        ";
            if ((($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "multiple", array(), "any", true, true) && ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "multiple") == true)) && twig_test_iterable((isset($context["value"]) ? $context["value"] : null)))) {
                // line 17
                echo "
            ";
                // line 18
                $context["result"] = "";
                // line 19
                echo "            ";
                $context["delimiter"] = (($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "delimiter", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "delimiter"), ", ")) : (", "));
                // line 20
                echo "
            ";
                // line 21
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["val"]) {
                    // line 22
                    echo "                ";
                    if ((!twig_test_empty((isset($context["result"]) ? $context["result"] : null)))) {
                        // line 23
                        echo "                    ";
                        $context["result"] = ((isset($context["result"]) ? $context["result"] : null) . (isset($context["delimiter"]) ? $context["delimiter"] : null));
                        // line 24
                        echo "                ";
                    }
                    // line 25
                    echo "
                ";
                    // line 26
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "choices", array(), "any", false, true), (isset($context["val"]) ? $context["val"] : null), array(), "array", true, true)) {
                        // line 27
                        echo "                    ";
                        if ((!$this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true))) {
                            // line 28
                            echo "                        ";
                            $context["result"] = ((isset($context["result"]) ? $context["result"] : null) . $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "choices"), (isset($context["val"]) ? $context["val"] : null), array(), "array"));
                            // line 29
                            echo "                    ";
                        } else {
                            // line 30
                            echo "                        ";
                            $context["result"] = ((isset($context["result"]) ? $context["result"] : null) . $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "choices"), (isset($context["val"]) ? $context["val"] : null), array(), "array"), array(), $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "catalogue")));
                            // line 31
                            echo "                    ";
                        }
                        // line 32
                        echo "                ";
                    } else {
                        // line 33
                        echo "                    ";
                        $context["result"] = ((isset($context["result"]) ? $context["result"] : null) . (isset($context["val"]) ? $context["val"] : null));
                        // line 34
                        echo "                ";
                    }
                    // line 35
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['val'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 36
                echo "
            ";
                // line 37
                $context["value"] = (isset($context["result"]) ? $context["result"] : null);
                // line 38
                echo "
        ";
            } elseif (twig_in_filter((isset($context["value"]) ? $context["value"] : null), twig_get_array_keys_filter($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "choices")))) {
                // line 40
                echo "            ";
                if ((!$this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true))) {
                    // line 41
                    echo "                ";
                    $context["value"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "choices"), (isset($context["value"]) ? $context["value"] : null), array(), "array");
                    // line 42
                    echo "            ";
                } else {
                    // line 43
                    echo "                ";
                    $context["value"] = $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "choices"), (isset($context["value"]) ? $context["value"] : null), array(), "array"), array(), $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "catalogue"));
                    // line 44
                    echo "            ";
                }
                // line 45
                echo "        ";
            }
            // line 46
            echo "    ";
        }
        // line 47
        echo "
    ";
        // line 48
        if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options"), "safe")) {
            // line 49
            echo "        ";
            echo (isset($context["value"]) ? $context["value"] : null);
            echo "
    ";
        } else {
            // line 51
            echo "        ";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "
    ";
        }
        // line 53
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_choice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  648 => 236,  637 => 234,  633 => 233,  625 => 230,  614 => 226,  612 => 225,  607 => 222,  597 => 218,  590 => 214,  586 => 212,  583 => 211,  581 => 210,  572 => 208,  568 => 207,  564 => 206,  560 => 205,  554 => 204,  545 => 200,  538 => 197,  535 => 196,  532 => 195,  527 => 186,  523 => 172,  520 => 171,  506 => 161,  502 => 160,  499 => 159,  494 => 129,  483 => 127,  479 => 126,  468 => 121,  459 => 119,  456 => 118,  433 => 96,  421 => 131,  419 => 118,  415 => 116,  410 => 94,  401 => 173,  359 => 146,  348 => 141,  343 => 138,  334 => 134,  331 => 93,  328 => 92,  291 => 80,  288 => 79,  265 => 76,  253 => 71,  237 => 66,  202 => 58,  180 => 50,  392 => 107,  389 => 162,  383 => 104,  377 => 153,  354 => 95,  352 => 94,  342 => 91,  332 => 88,  326 => 91,  324 => 85,  318 => 83,  315 => 82,  302 => 76,  299 => 75,  293 => 73,  287 => 71,  284 => 70,  276 => 67,  271 => 78,  263 => 58,  257 => 56,  233 => 48,  194 => 40,  190 => 54,  146 => 33,  137 => 29,  129 => 25,  126 => 24,  12 => 36,  139 => 51,  118 => 32,  20 => 11,  157 => 56,  145 => 53,  1031 => 295,  1028 => 294,  1025 => 293,  1021 => 326,  1017 => 324,  1011 => 321,  1008 => 320,  1006 => 319,  1000 => 316,  992 => 315,  989 => 314,  987 => 313,  984 => 312,  978 => 310,  976 => 309,  973 => 308,  967 => 306,  965 => 305,  962 => 304,  956 => 302,  954 => 301,  951 => 300,  945 => 298,  943 => 297,  940 => 296,  938 => 293,  935 => 292,  932 => 291,  928 => 264,  922 => 261,  919 => 260,  916 => 259,  913 => 258,  909 => 285,  904 => 282,  896 => 277,  891 => 275,  887 => 273,  885 => 272,  881 => 270,  875 => 268,  873 => 267,  869 => 265,  867 => 258,  864 => 257,  861 => 256,  858 => 255,  853 => 286,  850 => 255,  847 => 254,  842 => 327,  840 => 291,  835 => 288,  833 => 254,  830 => 253,  827 => 252,  822 => 244,  819 => 243,  815 => 242,  811 => 240,  805 => 239,  800 => 236,  794 => 235,  782 => 233,  779 => 232,  775 => 231,  769 => 230,  762 => 227,  758 => 226,  750 => 224,  744 => 223,  741 => 222,  738 => 221,  735 => 220,  730 => 219,  727 => 218,  725 => 217,  722 => 216,  719 => 215,  712 => 214,  709 => 213,  706 => 212,  703 => 211,  697 => 210,  694 => 209,  691 => 208,  688 => 206,  681 => 205,  678 => 204,  672 => 203,  669 => 202,  665 => 201,  662 => 200,  659 => 199,  656 => 198,  650 => 197,  646 => 195,  632 => 186,  626 => 184,  623 => 183,  620 => 228,  616 => 246,  613 => 243,  610 => 198,  608 => 197,  605 => 196,  602 => 182,  599 => 181,  593 => 247,  591 => 181,  587 => 179,  584 => 178,  577 => 329,  575 => 209,  571 => 250,  569 => 178,  566 => 177,  563 => 176,  555 => 165,  552 => 164,  544 => 158,  541 => 198,  533 => 151,  530 => 150,  526 => 147,  522 => 145,  516 => 170,  513 => 169,  510 => 168,  496 => 140,  490 => 138,  486 => 136,  472 => 122,  470 => 131,  467 => 130,  464 => 120,  446 => 128,  443 => 127,  441 => 126,  438 => 125,  432 => 123,  428 => 172,  424 => 132,  422 => 150,  418 => 148,  416 => 123,  405 => 114,  399 => 168,  395 => 111,  391 => 109,  385 => 107,  382 => 106,  372 => 103,  367 => 149,  364 => 101,  361 => 100,  353 => 96,  349 => 93,  346 => 93,  338 => 112,  335 => 89,  333 => 93,  330 => 92,  327 => 91,  321 => 89,  317 => 87,  311 => 85,  297 => 84,  292 => 82,  289 => 81,  282 => 69,  279 => 68,  262 => 76,  259 => 72,  250 => 70,  248 => 71,  242 => 67,  239 => 68,  234 => 64,  231 => 65,  225 => 61,  222 => 44,  205 => 59,  192 => 50,  185 => 52,  181 => 44,  167 => 43,  96 => 30,  52 => 6,  175 => 43,  150 => 35,  74 => 29,  58 => 23,  34 => 16,  114 => 34,  110 => 41,  65 => 29,  97 => 24,  81 => 33,  76 => 31,  53 => 15,  281 => 123,  256 => 74,  236 => 94,  232 => 93,  216 => 86,  211 => 61,  200 => 79,  195 => 49,  188 => 73,  184 => 45,  172 => 66,  152 => 61,  124 => 181,  120 => 46,  83 => 32,  186 => 62,  161 => 6,  37 => 19,  23 => 11,  170 => 44,  160 => 50,  155 => 55,  134 => 28,  127 => 182,  113 => 42,  104 => 43,  100 => 39,  90 => 24,  84 => 34,  77 => 30,  70 => 22,  480 => 134,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 124,  430 => 95,  427 => 143,  423 => 142,  413 => 95,  409 => 132,  407 => 93,  402 => 113,  398 => 129,  393 => 164,  387 => 159,  384 => 158,  381 => 120,  379 => 119,  374 => 101,  368 => 99,  365 => 98,  362 => 147,  360 => 109,  355 => 145,  341 => 137,  337 => 135,  322 => 101,  314 => 86,  312 => 98,  309 => 83,  305 => 77,  298 => 91,  294 => 81,  285 => 79,  283 => 88,  278 => 86,  268 => 77,  264 => 115,  258 => 81,  252 => 53,  247 => 107,  241 => 77,  229 => 73,  220 => 87,  214 => 62,  177 => 65,  169 => 33,  140 => 30,  132 => 51,  128 => 47,  111 => 30,  107 => 40,  61 => 24,  273 => 118,  269 => 117,  254 => 92,  246 => 68,  243 => 105,  240 => 86,  238 => 85,  235 => 74,  230 => 47,  227 => 91,  224 => 45,  221 => 77,  219 => 64,  217 => 63,  208 => 60,  204 => 80,  179 => 70,  159 => 40,  143 => 56,  135 => 69,  131 => 48,  119 => 44,  108 => 45,  102 => 75,  71 => 28,  67 => 15,  63 => 25,  59 => 27,  47 => 20,  98 => 36,  93 => 39,  88 => 28,  78 => 19,  40 => 3,  94 => 40,  89 => 34,  85 => 36,  79 => 32,  75 => 18,  68 => 27,  56 => 26,  50 => 21,  27 => 14,  43 => 7,  28 => 13,  87 => 35,  72 => 28,  55 => 25,  41 => 6,  21 => 1,  201 => 92,  196 => 56,  183 => 61,  171 => 61,  166 => 32,  163 => 42,  158 => 56,  156 => 39,  151 => 30,  142 => 59,  138 => 187,  136 => 186,  123 => 52,  121 => 180,  117 => 178,  115 => 177,  105 => 76,  101 => 37,  91 => 39,  69 => 16,  66 => 26,  62 => 28,  49 => 23,  32 => 16,  38 => 5,  24 => 14,  46 => 8,  44 => 19,  35 => 2,  25 => 13,  31 => 14,  29 => 15,  26 => 14,  22 => 12,  19 => 11,  209 => 82,  203 => 78,  199 => 57,  193 => 55,  189 => 47,  187 => 53,  182 => 51,  176 => 48,  173 => 35,  168 => 69,  164 => 59,  162 => 68,  154 => 58,  149 => 51,  147 => 35,  144 => 32,  141 => 188,  133 => 49,  130 => 43,  125 => 46,  122 => 45,  116 => 43,  112 => 176,  109 => 43,  106 => 26,  103 => 38,  99 => 23,  95 => 19,  92 => 35,  86 => 33,  82 => 35,  80 => 31,  73 => 17,  64 => 32,  60 => 24,  57 => 23,  54 => 22,  51 => 24,  48 => 5,  45 => 4,  42 => 18,  39 => 17,  36 => 16,  33 => 15,  30 => 2,);
    }
}
