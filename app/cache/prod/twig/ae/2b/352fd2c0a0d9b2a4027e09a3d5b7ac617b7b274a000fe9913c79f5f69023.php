<?php

/* SonataAdminBundle:CRUD:edit_text.html.twig */
class __TwigTemplate_ae2b352fd2c0a0d9b2a4027e09a3d5b7ac617b7b274a000fe9913c79f5f69023 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((isset($context["base_template"]) ? $context["base_template"] : null));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : null), 'widget', array("attr" => array("class" => "title")));
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 52,  118 => 49,  20 => 11,  157 => 56,  145 => 54,  1031 => 295,  1028 => 294,  1025 => 293,  1021 => 326,  1017 => 324,  1011 => 321,  1008 => 320,  1006 => 319,  1000 => 316,  992 => 315,  989 => 314,  987 => 313,  984 => 312,  978 => 310,  976 => 309,  973 => 308,  967 => 306,  965 => 305,  962 => 304,  956 => 302,  954 => 301,  951 => 300,  945 => 298,  943 => 297,  940 => 296,  938 => 293,  935 => 292,  932 => 291,  928 => 264,  922 => 261,  919 => 260,  916 => 259,  913 => 258,  909 => 285,  904 => 282,  896 => 277,  891 => 275,  887 => 273,  885 => 272,  881 => 270,  875 => 268,  873 => 267,  869 => 265,  867 => 258,  864 => 257,  861 => 256,  858 => 255,  853 => 286,  850 => 255,  847 => 254,  842 => 327,  840 => 291,  835 => 288,  833 => 254,  830 => 253,  827 => 252,  822 => 244,  819 => 243,  815 => 242,  811 => 240,  805 => 239,  800 => 236,  794 => 235,  782 => 233,  779 => 232,  775 => 231,  769 => 230,  762 => 227,  758 => 226,  750 => 224,  744 => 223,  741 => 222,  738 => 221,  735 => 220,  730 => 219,  727 => 218,  725 => 217,  722 => 216,  719 => 215,  712 => 214,  709 => 213,  706 => 212,  703 => 211,  697 => 210,  694 => 209,  691 => 208,  688 => 206,  681 => 205,  678 => 204,  672 => 203,  669 => 202,  665 => 201,  662 => 200,  659 => 199,  656 => 198,  650 => 197,  646 => 195,  632 => 186,  626 => 184,  623 => 183,  620 => 182,  616 => 246,  613 => 243,  610 => 198,  608 => 197,  605 => 196,  602 => 182,  599 => 181,  593 => 247,  591 => 181,  587 => 179,  584 => 178,  577 => 329,  575 => 252,  571 => 250,  569 => 178,  566 => 177,  563 => 176,  555 => 165,  552 => 164,  544 => 158,  541 => 157,  533 => 151,  530 => 150,  526 => 147,  522 => 145,  516 => 143,  513 => 142,  510 => 141,  496 => 140,  490 => 138,  486 => 136,  472 => 132,  470 => 131,  467 => 130,  464 => 129,  446 => 128,  443 => 127,  441 => 126,  438 => 125,  432 => 123,  428 => 172,  424 => 170,  422 => 150,  418 => 148,  416 => 123,  405 => 114,  399 => 112,  395 => 111,  391 => 109,  385 => 107,  382 => 106,  372 => 103,  367 => 102,  364 => 101,  361 => 100,  353 => 96,  349 => 94,  346 => 93,  338 => 112,  335 => 100,  333 => 93,  330 => 92,  327 => 91,  321 => 90,  317 => 87,  311 => 85,  297 => 84,  292 => 82,  289 => 81,  282 => 78,  279 => 77,  262 => 76,  259 => 75,  250 => 72,  248 => 71,  242 => 69,  239 => 68,  234 => 64,  231 => 63,  225 => 61,  222 => 60,  205 => 53,  192 => 50,  185 => 46,  181 => 44,  167 => 42,  96 => 40,  52 => 21,  175 => 43,  150 => 56,  74 => 25,  58 => 23,  34 => 18,  114 => 34,  110 => 45,  65 => 26,  97 => 39,  81 => 38,  76 => 33,  53 => 24,  281 => 123,  256 => 74,  236 => 94,  232 => 93,  216 => 86,  211 => 56,  200 => 79,  195 => 49,  188 => 73,  184 => 45,  172 => 66,  152 => 61,  124 => 47,  120 => 46,  83 => 24,  186 => 62,  161 => 6,  37 => 20,  23 => 12,  170 => 57,  160 => 50,  155 => 55,  134 => 45,  127 => 63,  113 => 46,  104 => 43,  100 => 91,  90 => 30,  84 => 39,  77 => 25,  70 => 28,  480 => 134,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 124,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 113,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 104,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 173,  337 => 103,  322 => 101,  314 => 86,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 79,  283 => 88,  278 => 86,  268 => 85,  264 => 115,  258 => 81,  252 => 109,  247 => 107,  241 => 77,  229 => 73,  220 => 87,  214 => 57,  177 => 65,  169 => 33,  140 => 51,  132 => 51,  128 => 42,  111 => 40,  107 => 44,  61 => 24,  273 => 118,  269 => 117,  254 => 92,  246 => 90,  243 => 105,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 91,  224 => 71,  221 => 77,  219 => 58,  217 => 75,  208 => 55,  204 => 80,  179 => 70,  159 => 41,  143 => 56,  135 => 69,  131 => 31,  119 => 40,  108 => 38,  102 => 175,  71 => 31,  67 => 29,  63 => 28,  59 => 27,  47 => 23,  98 => 36,  93 => 39,  88 => 37,  78 => 34,  40 => 19,  94 => 34,  89 => 24,  85 => 36,  79 => 34,  75 => 36,  68 => 30,  56 => 25,  50 => 24,  27 => 14,  43 => 20,  28 => 14,  87 => 29,  72 => 28,  55 => 25,  41 => 23,  21 => 11,  201 => 92,  196 => 51,  183 => 61,  171 => 61,  166 => 32,  163 => 58,  158 => 56,  156 => 64,  151 => 50,  142 => 59,  138 => 33,  136 => 51,  123 => 52,  121 => 51,  117 => 44,  115 => 47,  105 => 43,  101 => 41,  91 => 37,  69 => 33,  66 => 23,  62 => 28,  49 => 21,  32 => 15,  38 => 16,  24 => 12,  46 => 18,  44 => 22,  35 => 15,  25 => 13,  31 => 16,  29 => 14,  26 => 14,  22 => 12,  19 => 11,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 47,  187 => 46,  182 => 66,  176 => 13,  173 => 35,  168 => 69,  164 => 59,  162 => 68,  154 => 58,  149 => 51,  147 => 35,  144 => 47,  141 => 58,  133 => 49,  130 => 43,  125 => 51,  122 => 44,  116 => 45,  112 => 56,  109 => 43,  106 => 37,  103 => 38,  99 => 41,  95 => 32,  92 => 31,  86 => 66,  82 => 35,  80 => 20,  73 => 32,  64 => 32,  60 => 26,  57 => 20,  54 => 16,  51 => 23,  48 => 22,  45 => 21,  42 => 17,  39 => 17,  36 => 17,  33 => 15,  30 => 14,);
    }
}
