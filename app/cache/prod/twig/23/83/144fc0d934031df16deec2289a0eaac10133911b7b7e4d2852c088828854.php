<?php

/* SonataAdminBundle::empty_layout.html.twig */
class __TwigTemplate_2383144fc0d934031df16deec2289a0eaac10133911b7b7e4d2852c088828854 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SonataAdminBundle::standard_layout.html.twig");

        $this->blocks = array(
            'sonata_header' => array($this, 'block_sonata_header'),
            'sonata_left_side' => array($this, 'block_sonata_left_side'),
            'sonata_nav' => array($this, 'block_sonata_nav'),
            'sonata_breadcrumb' => array($this, 'block_sonata_breadcrumb'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'sonata_wrapper' => array($this, 'block_sonata_wrapper'),
            'sonata_page_content' => array($this, 'block_sonata_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle::standard_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_sonata_header($context, array $blocks = array())
    {
    }

    // line 15
    public function block_sonata_left_side($context, array $blocks = array())
    {
    }

    // line 16
    public function block_sonata_nav($context, array $blocks = array())
    {
    }

    // line 17
    public function block_sonata_breadcrumb($context, array $blocks = array())
    {
    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 20
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .content {
            margin: 0px;
            padding: 0px;
        }
    </style>

";
    }

    // line 31
    public function block_sonata_wrapper($context, array $blocks = array())
    {
        // line 32
        echo "    ";
        $this->displayBlock('sonata_page_content', $context, $blocks);
    }

    public function block_sonata_page_content($context, array $blocks = array())
    {
        // line 33
        echo "        ";
        $this->displayParentBlock("sonata_page_content", $context, $blocks);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::empty_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 20,  175 => 74,  150 => 61,  74 => 30,  58 => 24,  34 => 14,  114 => 34,  110 => 42,  65 => 15,  97 => 39,  81 => 33,  76 => 21,  53 => 20,  281 => 123,  256 => 110,  236 => 94,  232 => 93,  216 => 86,  211 => 84,  200 => 79,  195 => 77,  188 => 73,  184 => 72,  172 => 66,  152 => 62,  124 => 45,  120 => 51,  83 => 24,  186 => 62,  161 => 6,  37 => 17,  23 => 1,  170 => 57,  160 => 54,  155 => 52,  134 => 55,  127 => 63,  113 => 37,  104 => 37,  100 => 40,  90 => 38,  84 => 21,  77 => 31,  70 => 29,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 115,  258 => 81,  252 => 109,  247 => 107,  241 => 77,  229 => 73,  220 => 87,  214 => 69,  177 => 65,  169 => 11,  140 => 52,  132 => 51,  128 => 47,  111 => 47,  107 => 35,  61 => 27,  273 => 118,  269 => 117,  254 => 92,  246 => 90,  243 => 105,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 91,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 80,  179 => 70,  159 => 61,  143 => 56,  135 => 69,  131 => 49,  119 => 42,  108 => 38,  102 => 53,  71 => 17,  67 => 25,  63 => 17,  59 => 13,  47 => 22,  98 => 31,  93 => 39,  88 => 37,  78 => 34,  40 => 8,  94 => 34,  89 => 20,  85 => 25,  79 => 18,  75 => 32,  68 => 14,  56 => 21,  50 => 23,  27 => 3,  43 => 18,  28 => 14,  87 => 25,  72 => 31,  55 => 25,  41 => 18,  21 => 2,  201 => 92,  196 => 90,  183 => 61,  171 => 61,  166 => 10,  163 => 63,  158 => 5,  156 => 64,  151 => 50,  142 => 59,  138 => 57,  136 => 51,  123 => 52,  121 => 59,  117 => 44,  115 => 42,  105 => 44,  101 => 42,  91 => 27,  69 => 32,  66 => 27,  62 => 24,  49 => 17,  32 => 4,  38 => 6,  24 => 4,  46 => 19,  44 => 16,  35 => 17,  25 => 3,  31 => 15,  29 => 14,  26 => 13,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 13,  173 => 65,  168 => 69,  164 => 59,  162 => 68,  154 => 58,  149 => 51,  147 => 52,  144 => 59,  141 => 58,  133 => 55,  130 => 41,  125 => 46,  122 => 45,  116 => 50,  112 => 56,  109 => 46,  106 => 33,  103 => 28,  99 => 41,  95 => 26,  92 => 38,  86 => 36,  82 => 33,  80 => 20,  73 => 29,  64 => 29,  60 => 23,  57 => 20,  54 => 19,  51 => 15,  48 => 9,  45 => 8,  42 => 21,  39 => 15,  36 => 17,  33 => 4,  30 => 3,);
    }
}
